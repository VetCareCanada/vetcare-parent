<?php
/**
 * Functions and definitions
 */

if (!function_exists('vetcare_theme_setup')) {
    function vetcare_theme_setup()
    {
        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * This theme does not use a hard-coded <title> tag in the document head,
         * WordPress will provide it for us.
         */
        add_theme_support('title-tag');

        /**
         * Add post-formats support.
         */
        add_theme_support(
            'post-formats',
            array(
                'link',
                'aside',
                'gallery',
                'image',
                'quote',
                'status',
                'video',
                'audio',
                'chat',
            )
        );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(1568, 9999);


        add_theme_support('custom-logo');


        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');


        register_nav_menus(
            array(
                'primary' => esc_html__('Primary menu', 'twentytwentyone'),
                'footer'  => __('Secondary menu', 'twentytwentyone'),
            )
        );

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        // Add support for Block Styles.
        add_theme_support('wp-block-styles');

        // Add support for full and wide align images.
        add_theme_support('align-wide');

        // Add support for editor styles.
        add_theme_support('editor-styles');

        //template editor
        add_theme_support('block-templates');
    }
}

add_action('after_setup_theme', 'vetcare_theme_setup');
