<?php

if (!function_exists('vetcare_phone')) {
    function vetcare_phone()
    {
        return get_field('phone', 'option');
    }
}
add_shortcode('phone', 'vetcare_phone');

if (!function_exists('vetcare_fax')) {
    function vetcare_fax()
    {
        return get_field('fax', 'option');
    }
}
add_shortcode('fax', 'vetcare_fax');

if (!function_exists('vetcare_address')) {
    function vetcare_address()
    {
        return get_field('address', 'option');
    }
}
add_shortcode('address', 'vetcare_address');

if (!function_exists('vetcare_email')) {
    function vetcare_email()
    {
        return get_field('email', 'option');
    }
}
add_shortcode('email', 'vetcare_email');

if (!function_exists('vetcare_social_links')) {
    function vetcare_social_links()
    {
        $facebook = '';
        $instagram = '';
        $tiktok = '';
        if (get_field("facebook", 'option')) {
            $facebook = ' <li><a target="_blank" href="' . get_field("facebook", 'option') . '"><i class="fab fa-facebook-f"></i></a></li>';
        }
        if (get_field("instagram", 'option')) {
            $instagram = ' <li><a target="_blank" href="' . get_field("instagram", 'option') . '"><i class="fab fa-instagram"></i></a></li>';
        }
        if (get_field("tiktok", 'option')) {
            $tiktok = ' <li><a target="_blank" href="' . get_field("tiktok", 'option') . '"><i class="fab fa-tiktok"></i></a></li>';
        }

        return '<ul class="social-links">' . $facebook . $instagram . $tiktok . '</ul>';

    }
}
add_shortcode('social_links', 'vetcare_social_links');

if (!function_exists('vetcare_year_shortcode')) {
    function vetcare_year_shortcode()
    {
        return date_i18n('Y');
    }
    add_shortcode('Y', 'vetcare_year_shortcode');
}