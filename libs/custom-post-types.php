<?php
// Register Custom Post Type Service
function create_service_cpt()
{

    $labels = array(
        'name' => _x('Services', 'Post Type General Name', 'vetcare'),
        'singular_name' => _x('Service', 'Post Type Singular Name', 'vetcare'),
        'menu_name' => _x('Services', 'Admin Menu text', 'vetcare'),
        'name_admin_bar' => _x('Service', 'Add New on Toolbar', 'vetcare'),
        'archives' => __('Service Archives', 'vetcare'),
        'attributes' => __('Service Attributes', 'vetcare'),
        'parent_item_colon' => __('Parent Service:', 'vetcare'),
        'all_items' => __('All Services', 'vetcare'),
        'add_new_item' => __('Add New Service', 'vetcare'),
        'add_new' => __('Add New', 'vetcare'),
        'new_item' => __('New Service', 'vetcare'),
        'edit_item' => __('Edit Service', 'vetcare'),
        'update_item' => __('Update Service', 'vetcare'),
        'view_item' => __('View Service', 'vetcare'),
        'view_items' => __('View Services', 'vetcare'),
        'search_items' => __('Search Service', 'vetcare'),
        'not_found' => __('Not found', 'vetcare'),
        'not_found_in_trash' => __('Not found in Trash', 'vetcare'),
        'featured_image' => __('Featured Image', 'vetcare'),
        'set_featured_image' => __('Set featured image', 'vetcare'),
        'remove_featured_image' => __('Remove featured image', 'vetcare'),
        'use_featured_image' => __('Use as featured image', 'vetcare'),
        'insert_into_item' => __('Insert into Service', 'vetcare'),
        'uploaded_to_this_item' => __('Uploaded to this Service', 'vetcare'),
        'items_list' => __('Services list', 'vetcare'),
        'items_list_navigation' => __('Services list navigation', 'vetcare'),
        'filter_items_list' => __('Filter Services list', 'vetcare'),
    );
    $args = array(
        'label' => __('Service', 'vetcare'),
        'description' => __('', 'vetcare'),
        'labels' => $labels,
        'menu_icon' => 'dashicons-store',
        'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions'),
        'taxonomies' => array(),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => false,
        'hierarchical' => false,
        'exclude_from_search' => false,
        'show_in_rest' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
    );
    register_post_type('vc_service', $args);

}

add_action('init', 'create_service_cpt', 0);


// Register Custom Post Type Team Member
function create_teammember_cpt()
{

    $labels = array(
        'name' => _x('Team Members', 'Post Type General Name', 'vetcare'),
        'singular_name' => _x('Team Member', 'Post Type Singular Name', 'vetcare'),
        'menu_name' => _x('Team Members', 'Admin Menu text', 'vetcare'),
        'name_admin_bar' => _x('Team Member', 'Add New on Toolbar', 'vetcare'),
        'archives' => __('Team Member Archives', 'vetcare'),
        'attributes' => __('Team Member Attributes', 'vetcare'),
        'parent_item_colon' => __('Parent Team Member:', 'vetcare'),
        'all_items' => __('All Team Members', 'vetcare'),
        'add_new_item' => __('Add New Team Member', 'vetcare'),
        'add_new' => __('Add New', 'vetcare'),
        'new_item' => __('New Team Member', 'vetcare'),
        'edit_item' => __('Edit Team Member', 'vetcare'),
        'update_item' => __('Update Team Member', 'vetcare'),
        'view_item' => __('View Team Member', 'vetcare'),
        'view_items' => __('View Team Members', 'vetcare'),
        'search_items' => __('Search Team Member', 'vetcare'),
        'not_found' => __('Not found', 'vetcare'),
        'not_found_in_trash' => __('Not found in Trash', 'vetcare'),
        'featured_image' => __('Featured Image', 'vetcare'),
        'set_featured_image' => __('Set featured image', 'vetcare'),
        'remove_featured_image' => __('Remove featured image', 'vetcare'),
        'use_featured_image' => __('Use as featured image', 'vetcare'),
        'insert_into_item' => __('Insert into Team Member', 'vetcare'),
        'uploaded_to_this_item' => __('Uploaded to this Team Member', 'vetcare'),
        'items_list' => __('Team Members list', 'vetcare'),
        'items_list_navigation' => __('Team Members list navigation', 'vetcare'),
        'filter_items_list' => __('Filter Team Members list', 'vetcare'),
    );
    $args = array(
        'label' => __('Team Member', 'vetcare'),
        'description' => __('', 'vetcare'),
        'labels' => $labels,
        'menu_icon' => 'dashicons-businessperson',
        'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions'),
        'taxonomies' => array(),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => false,
        'hierarchical' => false,
        'exclude_from_search' => false,
        'show_in_rest' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
    );
    register_post_type('vc_teammember', $args);

}

add_action('init', 'create_teammember_cpt', 0);

// Register Custom Post Type Testimonial
function create_testimonial_cpt()
{

    $labels = array(
        'name' => _x('Testimonials', 'Post Type General Name', 'textdomain'),
        'singular_name' => _x('Testimonial', 'Post Type Singular Name', 'textdomain'),
        'menu_name' => _x('Testimonials', 'Admin Menu text', 'textdomain'),
        'name_admin_bar' => _x('Testimonial', 'Add New on Toolbar', 'textdomain'),
        'archives' => __('Testimonial Archives', 'textdomain'),
        'attributes' => __('Testimonial Attributes', 'textdomain'),
        'parent_item_colon' => __('Parent Testimonial:', 'textdomain'),
        'all_items' => __('All Testimonials', 'textdomain'),
        'add_new_item' => __('Add New Testimonial', 'textdomain'),
        'add_new' => __('Add New', 'textdomain'),
        'new_item' => __('New Testimonial', 'textdomain'),
        'edit_item' => __('Edit Testimonial', 'textdomain'),
        'update_item' => __('Update Testimonial', 'textdomain'),
        'view_item' => __('View Testimonial', 'textdomain'),
        'view_items' => __('View Testimonials', 'textdomain'),
        'search_items' => __('Search Testimonial', 'textdomain'),
        'not_found' => __('Not found', 'textdomain'),
        'not_found_in_trash' => __('Not found in Trash', 'textdomain'),
        'featured_image' => __('Featured Image', 'textdomain'),
        'set_featured_image' => __('Set featured image', 'textdomain'),
        'remove_featured_image' => __('Remove featured image', 'textdomain'),
        'use_featured_image' => __('Use as featured image', 'textdomain'),
        'insert_into_item' => __('Insert into Testimonial', 'textdomain'),
        'uploaded_to_this_item' => __('Uploaded to this Testimonial', 'textdomain'),
        'items_list' => __('Testimonials list', 'textdomain'),
        'items_list_navigation' => __('Testimonials list navigation', 'textdomain'),
        'filter_items_list' => __('Filter Testimonials list', 'textdomain'),
    );
    $args = array(
        'label' => __('Testimonial', 'textdomain'),
        'description' => __('', 'textdomain'),
        'labels' => $labels,
        'menu_icon' => 'dashicons-admin-comments',
        'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
        'taxonomies' => array(),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => false,
        'hierarchical' => false,
        'exclude_from_search' => false,
        'show_in_rest' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
    );
    register_post_type('vc_testimonial', $args);

}

add_action('init', 'create_testimonial_cpt', 0);