<?php

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Site General Settings',
		'menu_title'	=> 'Site Settings',
		'menu_slug' 	=> 'site-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

    acf_add_options_page(array(
        'page_title' 	=> 'Theme Setup',
        'menu_title'	=> 'Theme Setup',
        'menu_slug' 	=> 'theme-setup',
        'capability'	=> 'edit_users',
        'redirect'		=> false
    ));
}