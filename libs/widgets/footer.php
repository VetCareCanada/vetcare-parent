<?php
/**
 * Register widget area.
 * @return void
 */
function vetcare_widgets_init()
{

    register_sidebar(
        array(
            'name'          => esc_html__('Footer 1', 'vetcare'),
            'id'            => 'vetcare-footer-1',
            'description'   => esc_html__('Add widgets here to appear in your footer.', 'vetcare'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );

    register_sidebar(
        array(
            'name'          => esc_html__('Footer 2', 'vetcare'),
            'id'            => 'vetcare-footer-2',
            'description'   => esc_html__('Add widgets here to appear in your footer.', 'vetcare'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );

    register_sidebar(
        array(
            'name'          => esc_html__('Footer 3', 'vetcare'),
            'id'            => 'vetcare-footer-3',
            'description'   => esc_html__('Add widgets here to appear in your footer.', 'vetcare'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );

    register_sidebar(
        array(
            'name'          => esc_html__('Footer 4', 'vetcare'),
            'id'            => 'vetcare-footer-4',
            'description'   => esc_html__('Add widgets here to appear in your footer.', 'vetcare'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );

    register_sidebar(
        array(
            'name'          => esc_html__('Footer 5', 'vetcare'),
            'id'            => 'vetcare-footer-5',
            'description'   => esc_html__('Add widgets here to appear in your footer.', 'vetcare'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );

    register_sidebar(
        array(
            'name'          => esc_html__('Footer Copyright text', 'vetcare'),
            'id'            => 'vetcare-footer-copyright',
            'description'   => esc_html__('Add widgets here to appear in your footer.', 'vetcare'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );
}
add_action('widgets_init', 'vetcare_widgets_init');
