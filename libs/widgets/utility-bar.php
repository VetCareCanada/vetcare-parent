<?php
/**
 * Register utility widget area.
 * @return void
 */
function vetcare_utility_widgets_init()
{

    register_sidebar(
        array(
            'name'          => esc_html__('utility bar 1', 'vetcare'),
            'id'            => 'utility-bar-1',
            'description'   => esc_html__('Add widgets here to appear in your utility bar.', 'vetcare'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );

    register_sidebar(
        array(
            'name'          => esc_html__('utility bar 2', 'vetcare'),
            'id'            => 'utility-bar-2',
            'description'   => esc_html__('Add widgets here to appear in your utility bar.', 'vetcare'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );

}
add_action('widgets_init', 'vetcare_utility_widgets_init');
