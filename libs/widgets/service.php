<?php
/**
 * Register widget area.
 * @return void
 */
function vetcare_design1_widgets_init()
{

    register_sidebar(
        array(
            'name' => esc_html__('Service Sidebar', 'vetcare'),
            'id' => 'vetcare-service-sidebar',
            'description' => esc_html__('Add widgets here to appear in your footer.', 'vetcare'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        )
    );

}

add_action('widgets_init', 'vetcare_design1_widgets_init');
