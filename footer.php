<?php
/**
 * The template for displaying the footer
 */
?>
</main><!-- #main -->
</div><!-- #primary -->
</div><!-- #content -->


<footer id="colophon" role="contentinfo">
    <div class="container">
        <div class="site-footer">
            <?php get_template_part('template-parts/footer/footer-widgets'); ?>
        </div>
    </div>
    <div class="site-copyright">
        <div class="container">
            <?php if (is_active_sidebar('vetcare-footer-copyright')) : ?>
                <aside class="widget-area">
                    <?php dynamic_sidebar('vetcare-footer-copyright'); ?>
                </aside><!-- .widget-area -->
            <?php endif; ?>
        </div>
    </div>
</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
