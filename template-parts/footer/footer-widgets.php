<?php
/**
 * Displays the footer widget area.
 */

if ( is_active_sidebar( 'vetcare-footer-1' ) ) : ?>
    <aside class="widget-area">
		<?php dynamic_sidebar( 'vetcare-footer-1' ); ?>
    </aside><!-- .widget-area -->
<?php endif; ?>
<?php if ( is_active_sidebar( 'vetcare-footer-2' ) ) : ?>
    <aside class="widget-area">
		<?php dynamic_sidebar( 'vetcare-footer-2' ); ?>
    </aside><!-- .widget-area -->
<?php endif; ?>
<?php if ( is_active_sidebar( 'vetcare-footer-3' ) ) : ?>
    <aside class="widget-area">
		<?php dynamic_sidebar( 'vetcare-footer-3' ); ?>
    </aside><!-- .widget-area -->
<?php endif; ?>
<?php if ( is_active_sidebar( 'vetcare-footer-4' ) ) : ?>
    <aside class="widget-area">
		<?php dynamic_sidebar( 'vetcare-footer-4' ); ?>
    </aside><!-- .widget-area -->
<?php endif; ?>
<?php if ( is_active_sidebar( 'vetcare-footer-5' ) ) : ?>
    <aside class="widget-area">
		<?php dynamic_sidebar( 'vetcare-footer-5' ); ?>
    </aside><!-- .widget-area -->
<?php endif; ?>
