<header id="masthead" role="banner">
    <nav class="navbar navbar-expand-xl navbar-light fixed-top main-navbar">
        <div class="container-fluid">


            <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
                <?php //if (has_custom_logo()) : ?>
                    <div class="site-logo">
                        <img class="main-logo" src="<?php echo esc_url(wp_get_attachment_url(get_theme_mod('custom_logo'))); ?>"/>
                        <?php if (get_field("site_white_logo", "option")) :
                            $siteWhiteLogo = get_field("site_white_logo", "option");
                            ?>
                            <img class="white-logo" title="<?php echo $siteWhiteLogo['title']; ?>"
                                 alt="<?php echo $siteWhiteLogo['alt']; ?>" src="<?php echo $siteWhiteLogo['url']; ?>"/>
                        <?php endif; ?>
                    </div>
                <?php //endif; ?>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#main-menu"
                    aria-controls="main-menu">
                <span class="navbar-toggler-icon"></span>
            </button>


            <div class="offcanvas offcanvas-end" tabindex="-1" id="main-menu" aria-labelledby="offcanvasNavbarLabel">
                <div class="offcanvas-header">
                    <?php if (has_custom_logo()) : ?>
                        <div class="site-logo">
                            <img src="<?php echo esc_url(wp_get_attachment_url(get_theme_mod('custom_logo'))); ?>"/>
                        </div>
                    <?php endif; ?>
                    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas"
                            aria-label="Close"></button>
                </div>
                <div class="offcanvas-body">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'primary',
                        'container' => true,
                        'menu_class' => '',
                        'fallback_cb' => '__return_false',
                        'items_wrap' => '<ul id="%1$s" class="navbar-nav me-auto mb-2 mb-md-0 %2$s">%3$s</ul>',
                        'depth' => 2,
                        'walker' => new bootstrap_5_wp_nav_menu_walker()
                    ));
                    ?>
                    <div class="row phone-and-social">
                        <div class="col d-xl-none"><a href="tel:<?php echo do_shortcode("[phone]") ?>"><i
                                        class="fas fa-phone"></i> <?php echo do_shortcode("[phone]") ?></a></div>
                        <div class="col text-end"><?php get_template_part('template-parts/common/site-social-links'); ?></div>
                    </div>
                    <?php if (get_field("main_nav_cta_button_url", "option")) :
                        $bigButtonLink = get_field("main_nav_cta_button_url", "option");
                        ?>
                        <div class="big-button">
                            <a class="btn btn-secondary" href="<?php echo $bigButtonLink['url']; ?>"
                               target="<?php echo $bigButtonLink['target']; ?>">
                                <?php echo $bigButtonLink['title']; ?>
                            </a>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </nav>
    <!-- <nav class="navbar navbar-expand-xl navbar-light fixed-top main-navbar">
        <div class="container-fluid">

                    <?php /*get_template_part('template-parts/header/site-branding'); */ ?>

                    <?php /*get_template_part('template-parts/header/site-nav'); */ ?>

            </div>


    </nav>-->
</header><!-- #masthead -->

