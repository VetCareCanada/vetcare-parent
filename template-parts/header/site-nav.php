<div class="collapse navbar-collapse" id="main-menu">
    <?php
    wp_nav_menu(array(
        'theme_location' => 'primary',
        'container' => true,
        'menu_class' => '',
        'fallback_cb' => '__return_false',
        'items_wrap' => '<ul id="%1$s" class="navbar-nav me-auto mb-2 mb-md-0 %2$s">%3$s</ul>',
        'depth' => 2,
        'walker' => new bootstrap_5_wp_nav_menu_walker()
    ));
    ?>

    <?php get_template_part('template-parts/common/site-social-links'); ?>
    <?php if (get_field("main_nav_cta_button_url", "option")) :
        $bigButtonLink = get_field("main_nav_cta_button_url", "option");?>
        <div class="big-button">
            <a class="btn btn-secondary" href="<?php echo $bigButtonLink['url']; ?>"  target="<?php echo $bigButtonLink['target']; ?>">
                <?php echo $bigButtonLink['title']; ?>
            </a>
        </div>
    <?php endif;?>
</div>
