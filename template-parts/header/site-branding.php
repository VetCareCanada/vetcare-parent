<a class="navbar-brand" href="<?php echo get_home_url(); ?>">
    <?php if (has_custom_logo()) : ?>
        <div class="site-logo">
            <img src="<?php echo esc_url(wp_get_attachment_url(get_theme_mod('custom_logo'))); ?>" />
        </div>
    <?php endif; ?>
</a>
<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main-menu"
        aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
