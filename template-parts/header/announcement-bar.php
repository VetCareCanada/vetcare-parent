<?php

if (get_field("enable_announcement_bar", "option")) {
    ?>
    <div class="announcement-bar-wrap" id="announcementBar">
        <div class="announcement-bar">
            <?php echo get_field("announcement_bar_copy", "option") ?>
        </div>
        <i class="fas fa-times" aria-hidden="true" onclick="document.getElementById('announcementBar').remove()"></i>
    </div>
    <?php
}
