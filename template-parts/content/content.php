<?php
/**
 * Template part for displaying posts
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="entry-content">
        <?php
        the_content();

        wp_link_pages(
            array(
                'before'   => '<nav class="page-links" aria-label="' . esc_attr__('Page', 'twentytwentyone') . '">',
                'after'    => '</nav>',
                /* translators: %: Page number. */
                'pagelink' => esc_html__('Page %', 'twentytwentyone'),
            )
        );

        ?>
    </div><!-- .entry-content -->

<!--    <footer class="entry-footer default-max-width">
        <?php /*twenty_twenty_one_entry_meta_footer(); */?>
    </footer>-->
    <!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
