<?php
/**
 * Template part for displaying posts
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<!--    <header class="entry-header alignwide">-->
<!--        --><?php //the_title('<h1 class="entry-title">', '</h1>'); ?>
<!--        --><?php //vc_post_thumbnail(); ?>
<!--    </header><!-- .entry-header -->

    <div class="entry-content">
        <?php
        the_content();

        wp_link_pages(
            array(
                'before'   => '<nav class="page-links" aria-label="' . esc_attr__('Page', 'vetcare') . '">',
                'after'    => '</nav>',
                /* translators: %: Page number. */
                'pagelink' => esc_html__('Page %', 'vetcare'),
            )
        );
        ?>
    </div><!-- .entry-content -->

    <?php if (! is_singular('attachment')) : ?>
        <?php get_template_part('template-parts/post/author-bio'); ?>
    <?php endif; ?>

</article><!-- #post-<?php the_ID(); ?> -->
