<ul class="social-links">
	<?php if ( get_field( "facebook" ,'option') ) : ?>
        <li><a target="_blank" href="<?php echo get_field( "facebook" ,'option') ?>"><i class="fab fa-facebook-f"></i></a></li>
	<?php endif; ?>

	<?php if ( get_field( "instagram" ,'option') ) : ?>
        <li><a target="_blank" href="<?php echo get_field( "instagram" ,'option') ?>"><i class="fab fa-instagram"></i></a></li>
	<?php endif; ?>

    <?php if ( get_field( "tiktok" ,'option') ) : ?>
        <li><a target="_blank" href="<?php echo get_field( "tiktok" ,'option') ?>"><i class="fab fa-tiktok"></i></a></li>
    <?php endif; ?>

</ul>
