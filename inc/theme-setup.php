<?php
if (!function_exists("get_theme_setup_style")) {
    function get_theme_setup_style($isEditor = false): void
    {
        echo '<style>';
        if ($isEditor === true) {
            echo '.editor-styles-wrapper {';
        } else {
            echo ':root {';
        }
        ?>

        --bs-primary: <?php echo get_field("primary_color", "option") ?>;
        --bs-secondary: <?php echo get_field("secondary_color", "option") ?>;
        --bs-tertiary: <?php echo get_field("tertiary_color", "option") ?>;
        --bs-dark: <?php echo get_field("dark_color", "option") ?>;
        --bs-white: <?php echo get_field("white_color", "option") ?>;

        --vc-primary-hover: <?php echo colourBrightness(get_field("primary_color", "option"), -0.3) ?>;
        --vc-secondary-hover: <?php echo colourBrightness(get_field("secondary_color", "option"), -0.3) ?>;
        --vc-tertiary-hover: <?php echo colourBrightness(get_field("tertiary_color", "option"), -0.3) ?>;

        --bs-body-font-family: "<?php echo get_field("secondary_font_name", "option") ?>", sans-serif;
        --bs-headings-font-family: "<?php echo get_field("primary_font_name", "option") ?>", sans-serif;

        --vc-navigation-button-bg-color: <?php echo get_field("navigation_button_bg_color", "option") ?>;
        --vc-navigation-button-text-color: <?php echo get_field("navigation_button_text_color", "option") ?>;
        --vc-navigation-button-bg-color-hover: <?php echo get_field("navigation_button_bg_color_hover", "option") ?>;
        --vc-navigation-button-text-color-hover: <?php echo get_field("navigation_button_text_color_hover", "option") ?>;
        --vc-navigation_bg_color: <?php echo get_field("navigation_bg_color", "option") ?>;
        --vc-navigation_bg_color_scrolled: <?php echo get_field("navigation_bg_color_scrolled", "option") ?>;
        --vc-navigation_link_color: <?php echo get_field("navigation_link_color", "option") ?>;
        --vc-navigation_link_color_hover: <?php echo get_field("navigation_link_color_hover", "option") ?>;
        --vc-navigation_link_store_color: <?php echo get_field("navigation_link_store_color", "option") ?>;
        --vc-navigation_link_store_color_hover: <?php echo get_field("navigation_link_store_color_hover", "option") ?>;
        --vc-navigation_dropdown_bg_color: <?php echo get_field("navigation_dropdown_bg_color", "option") ?>;
        --vc-navigation_dropdown_bg_color_hover: <?php echo get_field("navigation_dropdown_bg_color_hover", "option") ?>;
        --vc-navigation_dropdown_link_color: <?php echo get_field("navigation_dropdown_link_color", "option") ?>;
        --vc-navigation_dropdown_link_color_hover: <?php echo get_field("navigation_dropdown_link_color_hover", "option") ?>;

        --vc-footer_background_color: <?php echo get_field("footer_background_color", "option") ?>;
        --vc-footer_text_color: <?php echo get_field("footer_text_color", "option") ?>;
        --vc-footer_link_color: <?php echo get_field("footer_link_color", "option") ?>;
        --vc-footer_link_hover_color: <?php echo get_field("footer_link_hover_color", "option") ?>;

        <?php
        echo '} </style>';

    }
}

if (!function_exists("colourBrightness")) {
    function colourBrightness($hex, $percent)
    {
        // Work out if hash given
        $hash = '';
        if (stristr($hex, '#')) {
            $hex = str_replace('#', '', $hex);
            $hash = '#';
        }
        /// HEX TO RGB
        $rgb = [hexdec(substr($hex, 0, 2)), hexdec(substr($hex, 2, 2)), hexdec(substr($hex, 4, 2))];
        //// CALCULATE
        for ($i = 0; $i < 3; $i++) {
            // See if brighter or darker
            if ($percent > 0) {
                // Lighter
                $rgb[$i] = round($rgb[$i] * $percent) + round(255 * (1 - $percent));
            } else {
                // Darker
                $positivePercent = $percent - ($percent * 2);
                $rgb[$i] = round($rgb[$i] * (1 - $positivePercent)); // round($rgb[$i] * (1-$positivePercent));
            }
            // In case rounding up causes us to go to 256
            if ($rgb[$i] > 255) {
                $rgb[$i] = 255;
            }
        }
        //// RBG to Hex
        $hex = '';
        for ($i = 0; $i < 3; $i++) {
            // Convert the decimal digit to hex
            $hexDigit = dechex($rgb[$i]);
            // Add a leading zero if necessary
            if (strlen($hexDigit) == 1) {
                $hexDigit = "0" . $hexDigit;
            }
            // Append to the hex string
            $hex .= $hexDigit;
        }
        return $hash . $hex;
    }
}

