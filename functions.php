<?php
$template_path = get_template_directory();
$include_files = [
    '/libs/custom-post-types.php',
    '/libs/theme-setup.php',
    '/libs/acf-options-page.php',
    '/libs/bootstrap-5-navwalker.php',
    '/libs/widgets/footer.php',
    '/libs/widgets/utility-bar.php',
    //'/libs/widgets/service.php',
    '/libs/shortcodes.php',
    '/inc/template-functions.php',
    '/inc/template-tags.php',
    '/inc/theme-setup.php',
];

if ($include_files) {
    foreach ($include_files as $key => $value) {
        if (file_exists($template_path . $value)) {
            require_once($template_path . $value);
        }
    }
}


function pr($d)
{
    echo '<pre>';
    print_r($d);
    echo '</pre>';
}
