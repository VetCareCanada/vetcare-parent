<?php
/**
 * The template for displaying all single posts
 */

get_header();

/* Start the Loop */
while (have_posts()) :
    the_post();

    get_template_part('template-parts/content/content-single');

    $vc_next_label = esc_html__('Next', 'vetcare') . '<i class="fas fa-long-arrow-alt-right"></i>';
    $vc_previous_label = '<i class="fas fa-long-arrow-alt-left"></i>' . esc_html__('Previous ', 'vetcare');

    the_post_navigation(
        array(
            'next_text' => '<p class="post-nav">' . $vc_next_label . '</p>',
            'prev_text' => '<p class="post-nav">' . $vc_previous_label . '</p>',
        )
    );


endwhile; // End of the loop.

get_footer();
